﻿using System;
using System.Collections.Generic;
using System.Text;
using CSVParser;

namespace MainProgram
{
    public class App
    {
        public void PrintContents()
        {
            Parser.Parse();
            var users = Parser.Users;

            foreach(User u in users)
            {
                Console.WriteLine($"Email: {u.Email}");
                Console.WriteLine($"Identifier: {u.Identifier}");
                Console.WriteLine($"First name: {u.FirstName}");
                Console.WriteLine($"Last name: {u.LastName}\n");
            }
        }
    }
}
