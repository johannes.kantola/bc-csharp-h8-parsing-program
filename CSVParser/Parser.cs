﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CSVParser
{
    public static class Parser
    {
        public static List<User> Users { get; set; }
            = new List<User>();
        public static void Parse()
        {
            string path = "../../../users.csv";
            string[] contents = null;
            try
            {
                contents = File.ReadAllLines(path);
            }
            catch
            {
                Console.WriteLine("File not found");
                return;
            }

            var parsedContents = contents
                .Skip(1)
                .Select(l => l.Split(';'))
                .ToArray();

            foreach(string[] line in parsedContents)
            {
                Users.Add(new User
                {
                    Email = line[0],
                    Identifier = int.Parse(line[1]),
                    FirstName = line[2],
                    LastName = line[3]
                });
            }
        }
    }
}
