﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSVParser
{
    public class User
    {
        public string Email { get; set; }
        public int Identifier { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
